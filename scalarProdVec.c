#include "linear_algebra.h"

double scalarProdVec(vector a, vector b)
{  
 if((a.dimension != b.dimension))
 {
  printf("Vector dimensions not equal. Cannot assign the vectors. Exiting program. \n");
  exit (1);
 }	
 
 double scalar_product = 0; 
 double* va = a.u;
 double* vb = b.u;
 unsigned int dim = a.dimension; 
 int i;
 for (i = 0; i < dim; i++)
 {
  scalar_product = scalar_product + va[i] * vb[i];
 } 
 return scalar_product;
}


