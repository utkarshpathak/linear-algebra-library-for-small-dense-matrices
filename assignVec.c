#include "linear_algebra.h"

void assignVec(vector a, vector b)
{  
 if((a.dimension != b.dimension))
 {
  printf("Vector dimensions not equal. Cannot assign the vectors. Exiting program. \n");
  exit (1);
 }	

 double* va = a.u;
 double* vb = b.u;
 unsigned int dim = a.dimension; 
 int i;
 for (i = 0; i < dim; i++)
 {
  va[i] = vb[i];
 } 
}


