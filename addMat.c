#include "linear_algebra.h"

void addMat(matrix C, matrix A, matrix B)
{

 if((A.m != B.m) || (A.m != C.m) || (A.n != B.n) || (A.n != C.n))
 {
  printf("Matrix dimensions not equal. Cannot add the matrices. Exiting program. \n");
  exit (1);
 }

 unsigned int m = A.m;
 unsigned int n = A.n;
 
 double** a = A.a; 
 double** b = B.a;
 double** c = C.a;
 
 int i;
 int j;
 for (i = 0; i < m; i++)
 for (j = 0; j < n; j++)
 {
  c[i][j] = a[i][j] + b[i][j];	 
 }  
}


