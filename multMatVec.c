#include "linear_algebra.h"

void multMatVec(vector v, matrix A, vector u)
{

 if((A.n != u.dimension) || (A.m != v.dimension))
 {
  printf("Matrix vector dimensions not matching for multiplication. Exiting program. \n");
  exit (1);
 }

 unsigned int m = A.m;
 unsigned int n = A.n;
 
 double** a = A.a; 
 double* x = u.u;
 double* b = v.u;
 
 int i;
 int j;
 for (i = 0; i < m; i++)
 {
  double sum = 0;
  for (j = 0; j < n; j++)
  {
   sum = sum + a[i][j] * x[j];
  }
  b[i] = sum;	 
 }  
}


