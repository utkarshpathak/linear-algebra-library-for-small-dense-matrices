#include "linear_algebra.h"

matrix initNullMat(unsigned int m, unsigned int n)
{
 matrix A;  
 A.m = m;
 A.n = n;
 A.a = malloc(sizeof(double*)*m);
 double** a = A.a;
 int init_rows;
 for (init_rows = 0; init_rows < m; init_rows++)
 {
  a[init_rows] = calloc(n, sizeof(double));
 }   
 return A;   
}


