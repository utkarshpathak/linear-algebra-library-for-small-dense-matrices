#include "linear_algebra.h"

matrix initIMat(unsigned int dimension)
{
 matrix A;  
 A.m = dimension;
 A.n = dimension;

 unsigned int m = dimension;
 unsigned int n = dimension; 
 A.a = malloc(sizeof(double*)*m);
 double** a = A.a;
 int init_rows;
 for (init_rows = 0; init_rows < m; init_rows++)
 {
  a[init_rows] = calloc(n, sizeof(double));
 }

 int i;
 for (i = 0; i < m; i++) a[i][i] = 1;	  
 return A;
}


