#include "linear_algebra.h"

double twoNormVec(vector a)
{  
 double norm = 0; 
 double* va = a.u;
 unsigned int dim = a.dimension; 
 int i;
 for (i = 0; i < dim; i++)
 {
  norm = norm + va[i] * va[i];
 } 
 return sqrt(norm);
}


