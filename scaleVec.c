#include "linear_algebra.h"

void scaleVec(double c, vector a)
{  
 double* va = a.u;
 unsigned int dim = a.dimension; 
 int i;
 for (i = 0; i < dim; i++)
 {
  va[i] = c * va[i];
 } 
}


